Compu Movil RSJ

Compu Movil es una pequeña empresa de emprendimiento, el señor Rafael Salazar J
empezó con éste emprendimiento en el año 1998 por lo que cuenta ya con más de 20 años
de experencia.

Ha trabajado en el mantenimiento y reparación de equipos de computo desde 1996, se ha
especializado en las marcas Lenovo y Lexmark en los últimos 8 años, también ha dado
tutorias de mantenimiento y reparación de equipos de computo en varias academias  y 
con el fin de ampliar su cartera de clientes es que nace la idea de crear Compu Movil RSJ
la cual esta dirigida a la atención de sus clientes por medio de visitas a sus lugares
de residencia o trabajo.

Don Rafael no solo da mantenimiento de equipos de computo, sino que tambien ofrece
la posibilidad de adquirir tanto insumos como partes para la reparación de los equipos
asi como equipos completos con garantia real.

En Compu Movil RSJ, don Rafael le brindará con todo gusto su servicio de alta calidad
y buen trato.
